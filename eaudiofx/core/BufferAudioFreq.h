/**
 * @author Edouard DUPIN
 * 
 * @copyright 2014, Edouard DUPIN, all right reserved
 * 
 * @license BSD v3 (see license file)
 */

#ifndef __EAUDIOFX_BUFFER_AUDIO_FREQ_H__
#define __EAUDIOFX_BUFFER_AUDIO_FREQ_H__

#include <eaudiofx/core/BufferAudio.h>

namespace eaudiofx {
	class BufferAudioFreq : public eaudiofx::BufferAudio {
		public:
			BufferAudioFreq(eaudiofx::Block& _parent);
			virtual ~BufferAudioFreq() {};
	};
};

#endif


