/**
 * @author Edouard DUPIN
 * 
 * @copyright 2014, Edouard DUPIN, all right reserved
 * 
 * @license BSD v3 (see license file)
 */

#include <eaudiofx/core/BufferAudioFreq.h>

#undef __class__
#define __class__ "BufferAudioFreq"


eaudiofx::BufferAudioFreq::BufferAudioFreq(eaudiofx::Block& _parent) :
  eaudiofx::BufferAudio(_parent) {
	
}

